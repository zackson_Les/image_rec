﻿using System;
using System.Drawing;
using System.IO;
namespace Image_Proc {

    class Program {
        static int Height = 0;
        static int Width = 0;
        static void Main(string[] args) {
            Color blank = Color.FromArgb(0, 0, 0, 0);

            Color[,] Skins_arr = Get_Pixle_arr("skin-tones.png");
            Console.WriteLine("Enter Image filepath to check the skin-tone? :");
            //string img_path = Console.ReadLine();
            Color[,] arr = new Color[1, 1];
            string[] img_path = { "jack.jpg","me.jpg", "me_2.jpg", "me_3.jpg" };
            for (int h = 0; h < img_path.Length; h++) {
                arr = Get_Pixle_arr(img_path[h]);


                Bitmap where_face = new Bitmap(width: Width, height: Height);


                Bitmap output = new Bitmap(width: Width, height: Height);
                for (int x = 0; x < 15; x++) {
                    for (int i = 0; i < Width; i++) {
                        for (int j = 0; j < Height; j++) {
                            int r = Skins_arr[x, 0].R - arr[i, j].R;
                            int g = Skins_arr[x, 0].G - arr[i, j].G;
                            int b = Skins_arr[x, 0].B - arr[i, j].B;
                            if (r > 0 && g > 0 && b > 0 && r < 110 && g < 110 && b < 110) {
                                output.SetPixel(i, j, arr[i, j]);
                            }
                            else {
                                output.SetPixel(i, j, blank);
                            }
                        }
                        //Console.WriteLine("R: " + arr[i, j].R +  " G: " + arr[i, j].G + " B: " + arr[i, j].B + " A: " + arr[i, j].A);
                    }
                    output.Save("output" + x + ".png");
                }

                long max = 0;
                int what_is_that_thing = 0;

                for (int x = 0; x < 15; x++) {
                    FileStream fs = new FileStream("output" + x + ".png", FileMode.Open);
                    if (fs.Length > max && x > 0) {
                        max = fs.Length;
                        what_is_that_thing = x;
                    }
                    fs.Close();
                    fs = null;
                }


                Console.WriteLine(what_is_that_thing);
                int[,] count = new int[Width, Height];
                for (int x = 0; x < what_is_that_thing; x++) {
                    arr = Get_Pixle_arr("output" + x + ".png");
                    for (int m = 0; m < what_is_that_thing; m++) {
                        Color[,] test = Get_Pixle_arr("output" + m + ".png");
                        for (int i = 0; i < Width; i++) {
                            for (int j = 0; j < Height; j++) {
                                if (arr[i, j].R == test[i, j].R && arr[i, j].B == test[i, j].B && arr[i, j].G == test[i, j].G && arr[i, j].A > 0 && test[i, j].A > 0 && test[i, j] != blank && arr[i, j] != blank)
                                    count[i, j]++;
                            }
                        }
                        test = null;
                    }

                    //face-test.jpg

                }

                int face_place = 20 - what_is_that_thing;
                Color[,] the_most_detailed_filter = Get_Pixle_arr("output" + what_is_that_thing + ".png");
                for (int b = 0; b < Width; b++) {
                    for (int g = 0; g < Height; g++) {
                        //Console.WriteLine(count[b, g]);
                        //Console.Write(count[b, g]);
                        if (count[b, g] >= face_place) {
                            where_face.SetPixel(b, g, arr[b, g]);

                            //count[b, g] = 0;
                        }
                        else {
                            // where_face.SetPixel(b, g, the_most_detailed_filter[b, g]);
                        }
                        //Console.WriteLine();

                    }
                }
                the_most_detailed_filter = null;
                where_face.Save("out.png");

                //Console.WriteLine("this is pobly: " + what_is_that_thing);

                get_obj_in_img(where_face, Get_Pixle_arr(img_path[h]), h);
            }

        }

        public static void get_obj_in_img(Bitmap img, Color[,] org_img, int num) {
            int first_i = 0;
            int first_j = 0;
            int last_i = 0;
            int last_j = 0;
            bool inti = false;
            Color blank = Color.FromArgb(0, 0, 0, 0);
            Bitmap start_end = new Bitmap(img.Width, img.Height);
            for (int i = 0; i < img.Width; i=+2) {
                for (int j = 0; j < img.Height; j=+2) {
                    Color tmp = img.GetPixel(i, j);
                    if (tmp != blank && !inti && i < img.Width && j < img.Height) {
                        //start_end.SetPixel(i, j, Color.FromArgb(255, 255, 0, 0));
                        int seeing = 0;
                        for (int c = 0; c < 3; c++) {
                            Color check_tmp_i_f = img.GetPixel(i + c, j);
                            Color check_tmp_j_f = img.GetPixel(i, j + c);
                            Color check_tmp_i_b = img.GetPixel(i - c, j);
                            Color check_tmp_j_b = img.GetPixel(i, j - c);
                            if (check_tmp_i_f != blank && check_tmp_j_f != blank && check_tmp_i_b != blank && check_tmp_j_b != blank) {
                                seeing++;
                            }
                        }
                        //Console.WriteLine("enter: " + seeing);
                        if (seeing == 3) {
                            first_i = i;
                            first_j = j;
                            inti = true;
                        }
                    }



                    if (tmp != blank && first_i != 0 && first_j != 0 && i < img.Width && j < img.Height) {
                        // start_end.SetPixel(i, j, Color.FromArgb(255, 0, 255, 0));
                        int seeing = 0;
                        for (int c = 0; c < 3; c++) {
                            Color check_tmp_i_f = img.GetPixel(i + c, j);
                            Color check_tmp_j_f = img.GetPixel(i, j + c);
                            Color check_tmp_i_b = img.GetPixel(i - c, j);
                            Color check_tmp_j_b = img.GetPixel(i, j - c);
                            if (check_tmp_i_f == blank && check_tmp_j_f == blank && check_tmp_i_b == blank && check_tmp_j_b == blank) {
                                seeing++;
                            }
                        }
                        //Console.WriteLine("exit: " + seeing);
                        if (seeing == 3) {
                            last_i = i;
                            last_j = j;
                        }
                    }
                    start_end.SetPixel(i, j, org_img[i, j]);
                }
            }

            for (int i = 0; i < img.Width; i++) {
                for (int j = 0; j < img.Height; j++) {
                    if ((i >= first_i && j == first_j) && i <= last_i) {
                        start_end.SetPixel(i, j, Color.FromArgb(255, 0, 255, 0));

                    }

                    if ((j == last_j && i <= last_i) && i >= first_i) {
                        start_end.SetPixel(i, j, Color.FromArgb(255, 0, 255, 0));
                    }

                    if ((i == first_i && j >= first_j && j <= last_j)) {
                        start_end.SetPixel(i, j, Color.FromArgb(255, 0, 255, 0));

                    }

                    if ((j <= last_j && i == last_i) && j >= first_j) {
                        start_end.SetPixel(i, j, Color.FromArgb(255, 0, 255, 0));
                    }

                }
            }

            start_end.Save("something_is_here" + num + ".png");

        }

        public static Color[,] Get_Pixle_arr(string path_to_img) {

            Bitmap img = new Bitmap(path_to_img);
            Width = img.Width;
            Height = img.Height;

            Color[,] ret = new Color[Width, Height];

            for (int i = 0; i < Width; i++) {
                for (int j = 0; j < Height; j++) {
                    ret[i, j] = img.GetPixel(i, j);
                }
            }

            img.Dispose();
            return ret;
        }
    }
}
